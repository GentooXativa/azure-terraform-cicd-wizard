# Environment definition
environment = "dev"

# Azure module configuration
resource_group_name     = "testing-development-rg"
resource_group_location = "West Europe"

tenant_id       = "002665ee-f642-4338-aa10-a51df6e11e90"
subscription_id = "60af4bb1-b509-49fa-8e04-a5ed50f34500"
