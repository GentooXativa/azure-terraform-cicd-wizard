terraform {
  backend "azurerm" {
    storage_account_name = "myrandomname"
    container_name       = "tfstate"
  }

  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.40.0"
    }

    random = {
      source  = "hashicorp/random"
      version = "~> 2.3.0"
    }
  }
}

# Configure the Azure Provider
provider "azurerm" {
  tenant_id = var.tenant_id

  features {}
}

# Resource group
resource "azurerm_resource_group" "main" {
  name     = var.resource_group_name
  location = var.resource_group_location
}
