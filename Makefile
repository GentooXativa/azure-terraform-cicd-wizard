plan:
	@echo "Starting terraform plan"
	@bin/terraform-plan

init:
	@echo "Initializing/Updating terraform modules"
	@bin/terraform-init
